package cz.morosystems.schlemmer.service;

import cz.morosystems.schlemmer.entity.User;
import org.springframework.security.access.prepost.PreAuthorize;

public interface IUserService {

    User getUserById(Long userId);

    Iterable<User> getAllUsers();

    User save(User user);

    @PreAuthorize("#user.getUserName() == principal.username")
    void delete(User user);
}
