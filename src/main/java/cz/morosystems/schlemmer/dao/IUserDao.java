package cz.morosystems.schlemmer.dao;

import cz.morosystems.schlemmer.entity.User;

public interface IUserDao {

    User getUserByUsername(String username);

    User getUserById(Long userId);

    Iterable<User> getAllUsers();

    User save(User user);

    void delete(User user);
}
