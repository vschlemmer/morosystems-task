package cz.morosystems.schlemmer.dao;

import cz.morosystems.schlemmer.entity.User;
import cz.morosystems.schlemmer.repository.IUserRepository;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;

@Transactional
@Repository
public class UserDao implements IUserDao {
    private IUserRepository repository;

    public UserDao(IUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User getUserByUsername(String username) {
        return repository.findByUserName(username);
    }

    @Override
    public User getUserById(Long userId) {
        return repository.findOne(userId);
    }

    @Override
    public Iterable<User> getAllUsers() {
        return repository.findAll();
    }

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public void delete(User user) {
        repository.delete(user.getId());
    }
}
