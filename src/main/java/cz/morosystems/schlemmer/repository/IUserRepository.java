package cz.morosystems.schlemmer.repository;

import cz.morosystems.schlemmer.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User, Long> {

    User findByUserName(String userName);

}
