package cz.morosystems.schlemmer.rest;

import cz.morosystems.schlemmer.entity.User;
import cz.morosystems.schlemmer.service.IUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
public class UserRestController {
    private IUserService userService;

    public UserRestController(IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/users")
    public Iterable<User> getUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping(path="/user/{id}", method=RequestMethod.GET)
    public ResponseEntity<ResponseBody<User>> getUser(@PathVariable Long id) {
        User user = userService.getUserById(id);
        if (user == null) {
            return userNotFound(id);
        }

        return ResponseEntity.ok(new ResponseBody<>(user));
    }

    @RequestMapping(path="/user/update", method=RequestMethod.POST)
    public ResponseEntity<ResponseBody<User>> updateUser(
            @Valid @RequestBody User user,
            BindingResult bindingResult) {
        if (userService.getUserById(user.getId()) == null) {
            return userNotFound(user.getId());
        }

        if (bindingResult.hasErrors()) {
            return validationErrors(bindingResult);
        }

        return ResponseEntity.ok(new ResponseBody<>(userService.save(user)));
    }

    @RequestMapping(path="/user/add", method=RequestMethod.POST)
    public ResponseEntity<ResponseBody<User>> addUser(
            @Valid @RequestBody User user,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return validationErrors(bindingResult);
        }

        return ResponseEntity.ok(new ResponseBody<>(userService.save(user)));
    }

    private ResponseEntity<ResponseBody<User>> validationErrors(BindingResult bindingResult) {
        String errorMessage = bindingResult.getAllErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.joining(","));
        return ResponseEntity.ok().body(new ResponseBody<>(null, errorMessage));
    }

    private ResponseEntity<ResponseBody<User>> userNotFound(Long id) {
        return ResponseEntity.badRequest().body(new ResponseBody<>(null, "Could not find user with id " + id + "."));
    }
}
