package cz.morosystems.schlemmer.rest;

public class ResponseBody<T> {
    private String msg;
    private T data;

    ResponseBody(T data) {
        this(data, "");
    }

    ResponseBody(T data, String msg) {
        this.msg = msg;
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
