package cz.morosystems.schlemmer.rest;

import cz.morosystems.schlemmer.entity.User;
import cz.morosystems.schlemmer.service.IUserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DeleteUserRestController {
    private IUserService userService;

    public DeleteUserRestController(IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(path="/secured/deleteUser/{id}", method=RequestMethod.GET)
    public ResponseEntity<ResponseBody<User>> deleteUser(@PathVariable Long id) {
        User user = userService.getUserById(id);
        if (user == null) {
            return ResponseEntity.badRequest().body(new ResponseBody<>(null, "Could not find user with id " + id + "."));
        }

        userService.delete(user);
        return ResponseEntity.ok(new ResponseBody<>(null));
    }
}
