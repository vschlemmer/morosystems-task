import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import UsersTable from './components/UsersTable';
import AddUser from './components/AddUser';
import UpdateUser from './components/UpdateUser';
import request from 'superagent';

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			users: [],
			updatingUser: null,
		};
	}

	componentDidMount() {
		request
			.get('/users')
			.end((err, res) => {
				if (!err) {
					this.setState({users: res.body});
				}
			});
	}

	handleDeleteUser(userId) {
		if (window.confirm('Do you really want to delete this user?')) {
			request
				.get('/secured/deleteUser/' + userId)
				.end((err, res) => {
					if (!err) {
						if (res.body.msg) {
							alert(res.body.msg);
						} else {
							this.setState({
								users: this.state.users.filter((user) => user.id !== userId),
							});
						}
					}
				});
		}
	}

	handleAddUser(name, userName, password) {
		request
			.post('/user/add')
			.send({
				name: name,
				userName: userName,
				password: password,
			})
			.then((res, err) => {
				if (!err) {
				} if (res.body.msg) {
					alert(res.body.msg);
				} else if (res.body.data) {
					this.setState({
						users: this.state.users.concat([res.body.data]),
					});
				}
			});
	}

	handleUpdateUserClicked(user) {
		this.setState({
			updatingUser: user,
		});
	}

	handleUpdateUser(user) {
		if (!this.state.updatingUser || !this.state.updatingUser.id) {
			return;
		}

		request
			.post('/user/update')
			.send(user)
			.then((res, err) => {
				if (!err) {
					if (res.body.msg) {
						alert(res.body.msg);
					} else if (res.body.data) {
						let users = this.state.users;
						for (let i in users) {
							if (users[i].id === res.body.data.id) {
								users[i] = res.body.data;
								break;
							}
						}

						this.setState({
							users: users,
							updatingUser: null,
						});
					}
				}
			});
	}

	cancelUpdate() {
		this.setState({
			updatingUser: null,
		});
	}

	renderCreateUpdate() {
		if (this.state.updatingUser && this.state.updatingUser.id) {
			return (
				<div>
					<span>Updating user with id {this.state.updatingUser.id}</span><br />
					<UpdateUser
						user={this.state.updatingUser}
						onUserUpdated={(user) => this.handleUpdateUser(user)} />
					<button
						className="btn btn-default"
						onClick={() => this.cancelUpdate()}>Cancel</button>
				</div>
			)
		} else {
			return (
				<AddUser onUserAdded={(name, userName, password) => this.handleAddUser(name, userName, password)} />
			)
		}
	}

	render() {
		const createUpdate = this.renderCreateUpdate();

		return (
			<div className="content">
				<UsersTable
					users={this.state.users}
					onRowDeleted={(userId) => this.handleDeleteUser(userId)}
					onRowUpdateClicked={(user) => this.handleUpdateUserClicked(user)}/>
				{createUpdate}
			</div>
		);
	}
}

ReactDOM.render(
	<Main/>,
	document.getElementById('root')
);
