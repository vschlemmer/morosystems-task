import React from 'react';
import PropTypes from 'prop-types';
import Row from './Row';

const Body = props => (
	<tbody>
	{props.users.map((user, index) =>
		<Row key={user.id}
			user={user}
			onRowDeleted={() => props.onRowDeleted(user.id)}
			onRowUpdateClicked={() => props.onRowUpdateClicked(user)}
		/>
	)}
	</tbody>
);

Body.propTypes = {
	users: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		userName: PropTypes.string,
		password: PropTypes.string,
	})),
	onRowDeleted: PropTypes.func,
	onRowUpdateClicked: PropTypes.func,
};

export default Body;
