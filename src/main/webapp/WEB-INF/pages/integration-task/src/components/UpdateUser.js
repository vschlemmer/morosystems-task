import React from 'react';
import TextField from './TextField';
import PropTypes from 'prop-types';

export default class UpdateUser extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			user: props.user,
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({user: nextProps.user});
	}

	onNameChange(value) {
		let user = this.state.user;
		user.name = value;
		this.setState({user: user});
	}

	onUserNameChange(value) {
		let user = this.state.user;
		user.userName = value;
		this.setState({user: user});
	}

	updateUserClicked() {
		if (!this.state.user.name) {
			alert('Insert name.');
			return;
		} else if (!this.state.user.userName) {
			alert('Insert username.');
			return;
		}
		this.props.onUserUpdated(this.state.user);
		this.onNameChange('');
		this.onUserNameChange('');
	};

	render() {
		return (
			<div>
				<TextField
					placeholder="Name"
					value={this.state.user.name}
					onValueChange={(value) => this.onNameChange(value)} />
				<TextField
					placeholder="Username"
					value={this.state.user.userName}
					onValueChange={(value) => this.onUserNameChange(value)} />
				<button
					className="btn btn-success"
					onClick={() => this.updateUserClicked()}>
					Update user
				</button>
			</div>
		);
	}
}

UpdateUser.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		userName: PropTypes.string,
		password: PropTypes.string,
	}),
	onUserUpdated: PropTypes.func,
};
