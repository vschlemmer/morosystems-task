import React from 'react';
import TextField from './TextField';
import PropTypes from 'prop-types';

export default class AddUser extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			name: '',
			userName: '',
			password: '',
		};
	}

	onNameChange(value) {
		this.setState({name: value});
	}

	onUserNameChange(value) {
		this.setState({userName: value});
	}

	onPasswordChange(value) {
		this.setState({password: value});
	}

	addUserClicked() {
		if (!this.state.name) {
			alert('Insert name.');
			return;
		} else if (!this.state.userName) {
			alert('Insert username.');
			return;
		} else if (!this.state.password) {
			alert('Insert password.');
			return;
		}

		this.props.onUserAdded(this.state.name, this.state.userName, this.state.password);
		this.onNameChange('');
		this.onUserNameChange('');
		this.onPasswordChange('');
	};

	render() {
		return (
			<div>
				<TextField
					placeholder="Name"
					value={this.state.name}
					onValueChange={(value) => this.onNameChange(value)} />
				<TextField
					placeholder="Username"
					value={this.state.userName}
					onValueChange={(value) => this.onUserNameChange(value)} />
				<TextField
					placeholder="Password"
					value={this.state.password}
					onValueChange={(value) => this.onPasswordChange(value)} />
				<button
					className="btn btn-default"
					onClick={() => this.addUserClicked()}>
					+ Add user
				</button>
			</div>
		);
	}
}

AddUser.propTypes = {
	onUserAdded: PropTypes.func,
};
