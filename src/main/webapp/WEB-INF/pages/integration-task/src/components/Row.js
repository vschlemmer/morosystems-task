import React from 'react';
import PropTypes from 'prop-types';
import Cell from './Cell';

const Row = props => {
	const renderActionButtons = (userId, onRowDeleted, onRowUpdateClicked) => (
		<span>
			<button className="btn btn-primary" onClick={onRowUpdateClicked}>Update</button>
			<button className="btn btn-warning" onClick={onRowDeleted}>Delete</button>
		</span>
	);

	const actionButtons = renderActionButtons(props.user.id, props.onRowDeleted, props.onRowUpdateClicked);

	return <tr>
		<Cell value={props.user.id}/>
		<Cell value={props.user.name}/>
		<Cell value={props.user.userName}/>
		<Cell value={actionButtons}/>
	</tr>
};

Row.propTypes = {
	user: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		userName: PropTypes.string,
		password: PropTypes.string,
	}),
	onRowDeleted: PropTypes.func,
	onRowUpdateClicked: PropTypes.func,
};

export default Row;
