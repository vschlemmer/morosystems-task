import React from 'react';
import PropTypes from 'prop-types';

const Cell = props => (<td>{props.value}</td>);

Cell.propTypes = {
	value: PropTypes.any,
};

export default Cell;
