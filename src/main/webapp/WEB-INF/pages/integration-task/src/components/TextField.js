import React from 'react';
import PropTypes from 'prop-types';
import Row from './Row';

const TextField = props => {
	const onValueChange = e => props.onValueChange(e.target.value);

	return <input
		type="text"
		onChange={onValueChange}
		value={props.value}
		placeholder={props.placeholder}/>;
};

Row.propTypes = {
	value: PropTypes.string,
	placeholder: PropTypes.string,
	onValueChange: PropTypes.func,
};

export default TextField;
