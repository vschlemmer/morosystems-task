import React from 'react';
import PropTypes from 'prop-types';
import Header from './Header';
import Body from './Body';

const UsersTable = props => (
	<div>
		<h1>Users management</h1>
		<table className="table">
			<Header/>
			<Body
				users={props.users}
				onRowDeleted={(userId) => props.onRowDeleted(userId)}
				onRowUpdateClicked={(user) => props.onRowUpdateClicked(user)}
			/>
		</table>
	</div>
);

UsersTable.propTypes = {
	users: PropTypes.arrayOf(PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		userName: PropTypes.string,
		password: PropTypes.string,
	})),
	onRowDeleted: PropTypes.func,
	onRowUpdateClicked: PropTypes.func,
};

export default UsersTable;
