Application description:
-------------------------
- the application enables to manage users

To run the application:
-------------------------
1) make sure you have a PostgreSQL database "morosystems" available at 127.0.0.1:5432 with table users, see the src/main/resources/scripts.sql file for the script to create the table  
2) `git clone https://vschlemmer@bitbucket.org/vschlemmer/morosystems-task.git`  
3) `cd morosystems-task`  
4) `mvn spring-boot:run`  
5) open new terminal and do `cd morosystems-task/src/main/webapp/WEB-INF/pages/integration-task`  
6) `npm install`  
7) `npm start`  
8) visit the app at localhost:3000  
